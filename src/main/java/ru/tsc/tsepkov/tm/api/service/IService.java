package ru.tsc.tsepkov.tm.api.service;

import ru.tsc.tsepkov.tm.api.repository.IRepository;
import ru.tsc.tsepkov.tm.model.AbstractModel;

public interface IService<M extends AbstractModel> extends IRepository<M> {

}
