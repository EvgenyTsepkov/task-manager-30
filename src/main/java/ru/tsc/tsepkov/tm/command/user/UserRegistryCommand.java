package ru.tsc.tsepkov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.tsepkov.tm.api.service.IAuthService;
import ru.tsc.tsepkov.tm.enumerated.Role;
import ru.tsc.tsepkov.tm.model.User;
import ru.tsc.tsepkov.tm.util.TerminalUtil;

public final class UserRegistryCommand extends AbstractUserCommand {

    @NotNull
    public static final String NAME = "user-registry";

    @NotNull
    public static final String DESCRIPTION = "Registry user.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[USER REGISTRY]");
        System.out.println("ENTER LOGIN");
        @NotNull final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        @NotNull final String password = TerminalUtil.nextLine();
        System.out.println("ENTER EMAIL:");
        @NotNull final String email = TerminalUtil.nextLine();
        @NotNull final IAuthService authService = serviceLocator.getAuthService();
        @Nullable final User user = authService.registry(login, password, email);
        if (user != null) {
            System.out.println("USER REGISTERED SUCCESSFULLY:");
            System.out.println("LOGIN: " + user.getLogin() + ";");
            System.out.println("ROLE:" + user.getRole() + ";");
            System.out.println("EMAIL: " + user.getEmail() + ".");
        }
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return null;
    }

}
