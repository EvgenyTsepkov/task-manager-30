package ru.tsc.tsepkov.tm.exception.field;

public final class EmailEmptyException extends AbstractFieldException {

    public EmailEmptyException() {
        super("Error! Email is empty...");
    }

}
