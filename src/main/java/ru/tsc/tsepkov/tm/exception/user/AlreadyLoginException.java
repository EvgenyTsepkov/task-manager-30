package ru.tsc.tsepkov.tm.exception.user;

public final class AlreadyLoginException extends AbstractUserException{

    public AlreadyLoginException() {
        super("Error! You are already logged in. Please log out and try again...");
    }

}
