package ru.tsc.tsepkov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.tsepkov.tm.api.repository.ICommandRepository;
import ru.tsc.tsepkov.tm.command.AbstractCommand;

import java.util.Collection;
import java.util.Map;
import java.util.TreeMap;

public final class CommandRepository implements ICommandRepository {

    @NotNull
    private final Map<String, AbstractCommand> mapByArgument = new TreeMap<>();

    @NotNull
    private final Map<String, AbstractCommand> mapByName = new TreeMap<>();

    @Override
    public void add(@Nullable AbstractCommand command) {
        if (command == null) return;
        @Nullable final String name = command.getName();
        if (name != null && !name.isEmpty()) mapByName.put(name, command);
        @Nullable final String argument = command.getName();
        if (argument != null && !argument.isEmpty()) mapByArgument.put(argument, command);
    }

    @Nullable
    @Override
    public AbstractCommand getCommandByArgument(@Nullable String argument) {
        if (argument == null || argument.isEmpty()) return null;
        return mapByArgument.get(argument);
    }

    @Nullable
    @Override
    public AbstractCommand getCommandByName(@Nullable String name) {
        if (name == null || name.isEmpty()) return null;
        return mapByName.get(name);
    }

    @NotNull
    @Override
    public Collection<AbstractCommand> getTerminalCommands() {
        return mapByName.values();
    }

    @NotNull
    @Override
    public Iterable<AbstractCommand> getCommandWithArgument() {
        return mapByArgument.values();
    }

}
